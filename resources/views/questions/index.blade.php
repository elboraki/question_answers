@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">All questions</div>

                <div class="card-body">
                    @foreach ($questions as $question)
                        <div class="media">
                            <div class="media-body">
                            <h3>{{$question->title}}</h3>
                            <p class="">{{str_limit($question->body,200)}}</p>    
                            </div>    
                        </div>   
                    @endforeach

                    {{$questions->links()}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
